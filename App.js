import React from 'react';
import Navigate from './src/routes/navigate'
import Onboarding from './src/screens/Onboarding/onboarding'
import firebase from '@react-native-firebase/app'
import Profile from './src/screens/Profile/profile'
import Register from './src/screens/Register/register';
import Home from './src/screens/Home/home';

var firebaseConfig = {
  apiKey: "AIzaSyAG7_--8tC_73wzVyOzTv7cjqQZf9woQNw",
  authDomain: "cakesinjar-b0cb8.firebaseapp.com",
  databaseURL: "https://cakesinjar-b0cb8.firebaseio.com",
  projectId: "cakesinjar-b0cb8",
  storageBucket: "cakesinjar-b0cb8.appspot.com",
  messagingSenderId: "787278024534",
  appId: "1:787278024534:web:270081aa9ae43dc4d3d0f7",
  measurementId: "G-JNTN82XTGK"
};

if(!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

const App = () => {
  return (
   <Navigate />
  // <Home />
  // <Profile />
  //  <Onboarding/>
  // <Register/>
  );
};

export default App;