import React from 'react';
import { Text, ImageBackground} from "react-native";

function Myorder ({navigation}) {
  return (
  <ImageBackground source={require('../../assets/image/background.png')} style={{ flex: 1, width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center'}}>
    <Text style={{fontSize: 30, fontWeight: 'bold', color: 'white'}}>My Order Page</Text>
  </ImageBackground>
  )
}

export default Myorder;