import React, { useEffect } from 'react';
import { View, Text, StyleSheet} from "react-native";
import MapboxGL from '@react-native-mapbox-gl/maps'

MapboxGL.setAccessToken('pk.eyJ1IjoibmlrZW5wY2RwIiwiYSI6ImNrZXp0NHZnODA3eXAzM3FhdDVxa3o0ZnEifQ.M7GZThhINPAruseCkxtxbA')

const Maps = () => {
  useEffect(() => {
    const getLocation = async() => {
      try{
        const permission = await MapboxGL.requestAndroidLocationPermissions()
      } catch(error) {
        console.log(error)
      }
    }
    getLocation()
  }, [])

  return (
    <View style={styles.container}>
      <MapboxGL.MapView
      style={{ flex: 1}}
      zoomEnabled={true}
      >
        <MapboxGL.UserLocation
          visible={true}
          />
          <MapboxGL.Camera
          followUserLocation={true}
          />
      </MapboxGL.MapView>

    </View>

  )
}

const styles=StyleSheet.create({
  container: {
    flex:1
  }
})

export default Maps;