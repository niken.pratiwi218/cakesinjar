import React from 'react';
import { Image, ImageBackground, StyleSheet, Text,ScrollView, TouchableOpacity, View} from "react-native";
import { TextInput } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'

function Home () {
  return (
  <ImageBackground source={require('../../assets/image/background.png')} style={{ flex: 1, width: '100%', height: '100%'}}>
    <View style={styles.searchSection}>
    <Icon style={styles.searchIcon} name="ios-search" size={20} color="#000"/>
    <TextInput
        style={styles.containerSearch}
        placeholder='Search here'
    />
    </View>
    <View style={styles.barFlavorVariants}>
      <ScrollView 
        horizontal={true}
        showsHorizontalScrollIndicator={false}
      >
      <TouchableOpacity style={styles.flavor}>
        <Image source={require('../../assets/image/choco.jpg')} style={styles.imageFlavor}/>
        <Text style={styles.titleFlavor}>Chocolate</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.flavor}>
        <Image source={require('../../assets/image/oreo.jpg')} style={styles.imageFlavor}/>
        <Text style={styles.titleFlavor}>Oreo</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.flavor}>
        <Image source={require('../../assets/image/peanut.jpg')} style={styles.imageFlavor}/>
        <Text style={styles.titleFlavor}>Peanut</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.flavor}>
        <Image source={require('../../assets/image/strawberry.jpg')} style={styles.imageFlavor}/>
        <Text style={styles.titleFlavor}>Strawberry</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.flavor}>
        <Image source={require('../../assets/image/cheese.jpg')} style={styles.imageFlavor}/>
        <Text style={styles.titleFlavor}>Cheese</Text>
      </TouchableOpacity>
      </ScrollView>
    </View>
    <View style={styles.discountSection}>
      <View style={styles.imageDiscountSection}>
      <Image source={require('../../assets/image/rainbowcakes.jpg')} style={{height: '100%', width: '100%', resizeMode: 'center'}} />
      </View>
      <View style={styles.detailDiscountSection}>
        <Text style={styles.titleDiscount}>35% Discount</Text>
        <Text style={styles.detailDiscountText}>Order any cake from app and get the discount</Text>
        <TouchableOpacity style={styles.buttonDiscount}>
          <Text style={styles.textOrder}>Order Now</Text>
        </TouchableOpacity>
      </View>
    </View>
    <Text style={styles.titleRecomended}>Recomended</Text>
    <ScrollView style={styles.recomendedSection}>
        <View style={styles.boxPinkRecomended}>
          <View style={styles.imageRecomendedSection}>
          <Image source={require('../../assets/image/chococakes.jpg')} style={{height: '100%', width: '100%', resizeMode: 'cover', borderRadius: 20}}/>
          </View>
          <View style={styles.detailRecomendedSection}>
            <MaterialIcons style={styles.iconFavorit} name='favorite' size={20} color='red'/>
            <Text style={styles.price}>Rp. 38.000,-</Text>
            <Text style={styles.nameCakesInjar}>Dark Chocolate Jar</Text>
            <TouchableOpacity style={styles.boxBuyPink}>
              <Text style={styles.titleBuy}>Add To Cart</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.boxPurpleRecomended}>
          <View style={styles.imageRecomendedSection}>
          <Image source={require('../../assets/image/oreocakes.jpg')} style={{height: '100%', width: '100%', resizeMode: 'cover', borderRadius: 20}}/>
          </View>
          <View style={styles.detailRecomendedSection}>
            <MaterialIcons style={styles.iconFavorit} name='favorite' size={20} color='red'/>
            <Text style={styles.price}>Rp. 35.000,-</Text>
            <Text style={styles.nameCakesInjar}>Oreo Mousse Jar</Text>
            <TouchableOpacity style={styles.boxBuyPurple}>
              <Text style={styles.titleBuy}>Add To Cart</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.boxPinkRecomended}>
          <View style={styles.imageRecomendedSection}>
          <Image source={require('../../assets/image/peanutcakes.jpg')} style={{height: '100%', width: '100%', resizeMode: 'cover', borderRadius: 20}}/>
          </View>
          <View style={styles.detailRecomendedSection}>
            <MaterialIcons style={styles.iconFavorit} name='favorite' size={20} color='red'/>
            <Text style={styles.price}>Rp. 30.000,-</Text>
            <Text style={styles.nameCakesInjar}>Peanut Butter Mason Jar</Text>
            <TouchableOpacity style={styles.boxBuyPink}>
              <Text style={styles.titleBuy}>Add To Cart</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.boxPurpleRecomended}>
          <View style={styles.imageRecomendedSection}>
          <Image source={require('../../assets/image/strawberrycakes.jpg')} style={{height: '100%', width: '100%', resizeMode: 'cover', borderRadius: 20}}/>
          </View>
          <View style={styles.detailRecomendedSection}>
            <MaterialIcons style={styles.iconFavorit} name='favorite' size={20} color='red'/>
            <Text style={styles.price}>Rp. 35.000,-</Text>
            <Text style={styles.nameCakesInjar}>Strawberry Shortcakes Jar</Text>
            <TouchableOpacity style={styles.boxBuyPurple}>
              <Text style={styles.titleBuy}>Add To Cart</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.boxPinkRecomended}>
          <View style={styles.imageRecomendedSection}>
          <Image source={require('../../assets/image/cheesecakes.jpg')} style={{height: '100%', width: '100%', resizeMode: 'cover', borderRadius: 20}}/>
          </View>
          <View style={styles.detailRecomendedSection}>
            <MaterialIcons style={styles.iconFavorit} name='favorite' size={20} color='red'/>
            <Text style={styles.price}>Rp. 40.000,-</Text>
            <Text style={styles.nameCakesInjar}>Lemon Curd Cheese Jar</Text>
            <TouchableOpacity style={styles.boxBuyPink}>
              <Text style={styles.titleBuy}>Add To Cart</Text>
            </TouchableOpacity>
          </View>
        </View>
    </ScrollView>
  </ImageBackground>
  )
}

const styles=StyleSheet.create({
  searchSection: {
    height: 50,
    width: 250,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    margin: 15,
    alignSelf: 'center',
    borderRadius: 20

} ,
  searchIcon: {
    padding: 10,
  },
  containerSearch: {
    flex: 1,
    backgroundColor: '#ffffff',
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    color: '#424242',
    borderRadius: 20
  },
  barFlavorVariants: {
    height: 80,
    marginBottom: 15,
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  flavor: {
    height: '100%',
    width: 100,
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    marginHorizontal: 10
  },
  imageFlavor: {
    height: '100%',
    width: '100%',
    opacity: .7,
    borderRadius: 30
  },
  titleFlavor: {
    position: 'absolute',
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'white',
    fontSize: 16
  },
  discountSection: {
    height: 150,
    width: '95%',
    alignSelf: 'center',
    borderRadius: 10,
    padding: 10,
    flexDirection: 'row',
    backgroundColor: '#fee0ff'
  },
  imageDiscountSection: {
    height: '100%',
    width: '40%',
    
  },
  detailDiscountSection: {
    height: '100%',
    width: '60%',
    justifyContent: 'center'
  },
  titleDiscount:{
    fontSize: 20,
    fontFamily: 'serif',
    fontWeight: 'bold'
  },
  detailDiscountText: {
    textAlign:'left',
    fontSize: 16,
    marginVertical: 10
  },
  buttonDiscount: {
    backgroundColor: '#a230a6',
    height: 30,
    width: 150,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textOrder: {
    color: 'white',
    fontWeight: 'bold'
  },
  titleRecomended: {
    color: 'red',
    fontSize: 18,
    fontWeight: 'bold',
    margin: 15
  },
  recomendedSection: {
    alignSelf: 'center',
    width: '95%',
    padding: 5,
  },
  boxPinkRecomended: {
    height: 150,
    width: '90%',
    backgroundColor: '#ffa8bd',
    borderRadius: 20,
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10
  },
  boxPurpleRecomended: {
    height: 150,
    width: '90%',
    backgroundColor: '#e9a8ff',
    borderRadius: 20,
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10
  },
  imageRecomendedSection :{
    height: '80%',
    width: '40%',
    backgroundColor: 'white',
    borderRadius: 20
  },
  detailRecomendedSection: {
    height: '80%',
    width: '55%',
    borderRadius: 20,
    padding: 10
  },
  iconFavorit: {
    alignSelf: 'flex-end'
  },
  price: {
    fontWeight: 'bold',
    fontSize: 16
  },
  nameCakesInjar:{
    fontSize: 14
  },
  boxBuyPurple: {
    alignSelf: 'center',
    height: 30,
    width: 100,
    backgroundColor: '#ffa8bd',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    marginTop: 10
  },
  boxBuyPink: {
    alignSelf: 'center',
    height: 30,
    width: 100,
    backgroundColor: '#e9a8ff',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    marginTop: 10
  },
  titleBuy: {
    fontWeight: 'bold',
    color: 'black'
  }
})
export default Home;