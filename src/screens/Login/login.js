import React, {useState, useEffect} from 'react';
import { View, Text, ImageBackground, Alert, TextInput, Button} from "react-native";
import styles from './style';
import Icon from 'react-native-vector-icons/Ionicons'
import { TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import api from '../../api';
import Axios from 'axios';
import auth from '@react-native-firebase/auth';
import { statusCodes, GoogleSignin, GoogleSigninButton } from '@react-native-community/google-signin'
import TouchID from 'react-native-touch-id'


const config = {
  title: 'Authentication Required',
  imageColor: '#191970',
  imageErrorColor: 'red',
  sensorDescription: 'Touch Sensor',
  sensorErrorDescription: 'Failed',
  cancelText: 'Cancel'
}
function Login ({navigation}) {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('') 

    const saveToken = async(token)=>{
        try{
            await AsyncStorage.setItem('token', token)
        } catch(err) {
            console.log(err)
        }
    }

    useEffect(()=> {
      configureGoogleSignIn()
    }, [])

    const configureGoogleSignIn = () => {
      GoogleSignin.configure({
          offlineAccess: false,
          webClientId: '787278024534-kratpl7po829tra1hi5ouuddd95060tp.apps.googleusercontent.com'
      })
    }

    const signInWithGoogle =  async () => {
      // const { idToken } = await GoogleSignin.signIn()
      // console.log(idToken)
      try{
          const { idToken } = await GoogleSignin.signIn()
          console.log('signInWithGoogle -> idToken', idToken)
          
          const credential = auth.GoogleAuthProvider.credential(idToken);

          auth().signInWithCredential(credential)
          await AsyncStorage.setItem('google', "true")
          await AsyncStorage.setItem('login', 'true')

          navigation.reset({
            index: 0,
            routes: [{ name: 'TabsScreen'}],
          })

      } catch (error) {
          console.log('signInWithGoogle -> error', error)
      }
    }

    const onLoginPress = () => {
      return auth().signInWithEmailAndPassword(email, password)
      .then((res) => {
        navigation.reset({
          index: 0,
          routes: [{ name: 'TabsScreen'}],
      })
      })
      .catch((err) => {
          console.log('onLoginPress -> err', err)
      })
  }

    // const onLoginPress = async() => {
    //   let data = {
    //       email: email,
    //       password: password
    //   }
    //   console.log(data)
    //   Axios.post(`${api}/login`, data, {
    //       timeout: 20000
    //   })
    //   .then(async(res)=> {
    //       saveToken(res.data.token)
    //       await AsyncStorage.setItem('login', 'true')
    //       navigation.reset({
    //         index: 0,
    //         routes: [{ name: 'TabsScreen'}],
    //       })
    //   })
    //   .catch((err) => {
    //       console.log(Alert.alert('Login Failed', 'Your email or password is incorrect. Please try again.'))
    //   })        
    // }
    const signInWithFingerprint = async() => {
      TouchID.authenticate('', config)
      .then(success => {
        AsyncStorage.setItem('login', 'true')
        navigation.reset({
            index: 0,
            routes: [{ name: 'TabsScreen'}],
        })
      })
      .catch(error => {
          console.log(error)
          alert('Authentication Failed')
      })
      
    }

  return (
    <ImageBackground source={require('../../assets/image/cake2.jpg')} style={{ flex: 1, width: '100%', height: '100%', justifyContent: 'flex-end'}}>
      <View style={styles.boxContainer}>
        <Text style={{fontWeight: 'bold', color: 'white'}}>Username</Text>
        <TextInput
          value={email}
          placeholder= 'Username or Email'
          underlineColorAndroid= 'white'
          placeholderTextColor='white'
          onChangeText={(email) => setEmail(email)}
        />
        <Text style={{fontWeight: 'bold', color: 'white', marginTop: 10}}>Password</Text>
        <TextInput
          secureTextEntry
          value={password}
          placeholder='Password'
          underlineColorAndroid='white'
          placeholderTextColor='white'
          onChangeText={(password) => setPassword(password)}
        />
        <Button
          color='#840EE1'
          title='LOGIN'
          onPress={() => onLoginPress()}
        />
        <Text style={{color: 'white', textAlign: 'center', margin: 25}}> or login with </Text>
        <View style={styles.boxSosmed}>
          <View style={styles.sosmed}>
            <Icon 
            onPress={() => signInWithGoogle()}
            name='logo-google' 
            size={30} 
            color='white' />
          </View>
          <TouchableOpacity style={styles.sosmed}>
            <Icon 
              onPress={() => signInWithFingerprint()}
              name='finger-print' 
              size={30} 
              color='white' />
          </TouchableOpacity>
        </View>
        <Text style={{textAlign: 'center', marginTop: 50 }} >Don't have an account?
          <Text style={{color: 'white'}} onPress={()=> navigation.navigate('Register')}>  Sign Up</Text>
        </Text>
      </View>
    </ImageBackground>
  )
}

export default Login;