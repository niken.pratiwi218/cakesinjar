import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  boxContainer: {
    flex: 0.6,
    width: '100%',
    backgroundColor: 'rgba(224, 42, 227, 0.5)',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    padding: 20,
  },
  boxSosmed: {
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: 150,
    height: 50
  },
  sosmed: {
    width: 50,
    height: 50,
    borderRadius: 50,
    backgroundColor: '#840EE1',
    elevation: 2,
    alignItems: 'center',
    justifyContent: 'center'
  }
})

export default styles;