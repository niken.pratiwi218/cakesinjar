import React, { useEffect, useState } from 'react';
import { GiftedChat } from 'react-native-gifted-chat';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database'
import {ImageBackground} from 'react-native'

const Chat = ({ route, navigation }) => {
  
  const [messages, setMessages] = useState([])
  const [user, setUser] = useState({})

  useEffect(() => {
    const user = auth().currentUser;
    setUser(user)
    getData()
    return () => {
        const db = database().ref('messages')
        if(db) {
          db.off()
        } 
    }
  }, [])

  const getData = () => {
    database().ref('messages').limitToLast(20).on('child_added', snapshot => {
      const value = snapshot.val()
      setMessages(previousMessages => GiftedChat.append(previousMessages, value))
    })
  }

  const onSend = (( messages = [] ) => {
    for(let i=0; i < messages.length; i++) {
      database().ref('messages').push({
        _id: messages[i]._id,
        createdAt: database.ServerValue.TIMESTAMP,
        text: messages[i].text,
        user: messages[i].user
      })
    }
  })

  return(
  <ImageBackground source={require('../../assets/image/background.png')} style={{ flex: 1}}>
    <GiftedChat messages={messages}
      onSend={messages => onSend(messages)}
      user={{
        _id: user.uid,
        name: user.email,
        avatar: 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png'
      }}
    />
  </ImageBackground>
  )
}


export default Chat;