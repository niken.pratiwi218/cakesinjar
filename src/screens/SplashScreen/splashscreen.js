import React from 'react';
import { View, Image, StyleSheet, ImageBackground } from "react-native";
import styles from './style'

function SplashScreen() {
  return(
    <ImageBackground source={require('../../assets/image/background.png')} style={{ flex: 1, width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center'}}>
      <View style ={styles.logoContainer}>
        <Image source={require('../../assets/image/logo-tagline.png')} style={{resizeMode: 'center' }}/>
      </View>
    </ImageBackground>
  )
};

export default SplashScreen;