import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  slides: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    fontSize: 20,
    color: 'white',
    fontWeight:'bold',
  },
  text: {
    fontSize: 18,
    color: 'white',
    textAlign: 'center'
  },
  image:{
    width: 50,
    height: 50
  },
  boxOnBoarding:{
    height: 150,
    width: 250,
    borderRadius: 20,
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: 'rgba(224, 42, 227, 0.3)'
  },
  buttonCircle: {
    backgroundColor: 'white',
    height: 50,
    width: 50,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default styles;