import React from 'react';
import { View, Text, Image, StatusBar, ImageBackground } from 'react-native';
import styles from './style';
import AppSlider from 'react-native-app-intro-slider';
import Icon from 'react-native-vector-icons/AntDesign';
import AsyncStorage from '@react-native-community/async-storage'

const slides = [
  {
    key: 1,
    title: 'Choose a yummy cake',
    text: 'Happiness in every bite',
    image: require('../../assets/image/cake1.jpg')
  },
  {
    key: 2,
    title: 'Order your happiness',
    text: 'Fast delivery for every purchase',
    image: require('../../assets/image/order.jpg')
  }
];

const Onboarding = ({ navigation }) => {
  const renderItem = ({ item }) => {
    return (
      <ImageBackground source={item.image} style={{ flex: 1, width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center'}}>
        <View style={styles.boxOnBoarding}>
          <Text style={styles.title}>{item.title}</Text>
          <Text style={styles.text}>{item.text}</Text>
        </View>
      </ImageBackground>
    )
  }

  const onDone = async() => {
    await AsyncStorage.setItem('Onboarding', 'true')
    navigation.navigate('Login')
  }

  const renderDoneButton = () => {
    return (
        <View style={styles.buttonCircle}>
            <Icon
                name="login"
                size={30}
            />
        </View>
    );
};
 
return (
  <View style={{flex: 1}}>
    <StatusBar barStyle="dark-content"/>
    <View style={{ flex: 1}}>
      <AppSlider
        data={slides}
        onDone={onDone}
        renderItem={renderItem}
        renderDoneButton={renderDoneButton}
        keyExtractor={(item, index) => index.toString()}
        activeDotStyle={{ backgroundColor: 'white' }}
      />
    </View>
  </View>
)
};

export default Onboarding;