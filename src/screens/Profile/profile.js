import React, { useState, useEffect } from 'react';
import { View, Text, ImageBackground, Image, TouchableOpacity} from "react-native";
import styles from './style';
import Icon from 'react-native-vector-icons/Feather';
import Icon2 from 'react-native-vector-icons/MaterialIcons';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import { statusCodes, GoogleSignin } from '@react-native-community/google-signin';

function Profile ({navigation}) {
  const [userInfo, setUserInfo] = useState(null)

  useEffect(() => {
    async function getToken() {
      try{
        const token =await AsyncStorage.getItem('token')
        const login = await AsyncStorage.getItem('login')
        const google = await AsyncStorage.getItem('google')
      } catch (err){
        console.log(err)
      }
    }
    getToken()
    getCurrentUser()
  }, [userInfo])

  const getCurrentUser = async() => {
    const userInfo = await GoogleSignin.signInSilently()
    setUserInfo(userInfo)
  }

  const getVenue =(token) =>{
    Axios.get(`${api}/venues`, {
      timeout: 20000,
      headers: {
        'Authorization' : 'Bearer' + token
      }
    })
    .then((res) => {
      console.log('Profile -> res', res)
    })
    .catch((err)=> {
      console.log('Profile -> err', err)
    }) 
  }

  const onLogoutPress = async () =>{
    const google = await AsyncStorage.getItem('google')
    
    console.log(google)
    try{
      if (google === 'true'){
        await GoogleSignin.revokeAccess()
        await GoogleSignin.signOut()
        await AsyncStorage.removeItem('login')
        await AsyncStorage.removeItem('google')
        navigation.navigate ('Login')
      } else{
        await AsyncStorage.removeItem('login')
        await AsyncStorage.removeItem('token')
        navigation.navigate ('Login')
      }
    }catch(err){
      console.log(err)
    }
  }

  const profilephoto = require('../../assets/image/PhotoProfile.jpg')

  return (
    <ImageBackground source={require('../../assets/image/background.png')} style={{ flex: 1, width: '100%', height: '100%',alignItems: 'center', justifyContent: 'flex-end' }}>
      <Text style={styles.profile}>Profile</Text>
      <Image source={{uri:`${userInfo ? userInfo && userInfo.user && userInfo.user.photo : profilephoto}`}} style={styles.imageProfile}/>
      <Text style={styles.username}>{userInfo && userInfo.user && userInfo.user.name }</Text>
      <Text style={styles.email}>{userInfo && userInfo.user && userInfo.user.email} </Text>
      <View style={styles.loc}>
        <Icon2 name='location-on' size={20} color='white' />
        <Text style={styles.nameLoc}>Indonesia</Text>
      </View>
      <View style={styles.boxContent}>
        <View style={styles.containerMyAccount}>
          <Text style={styles.myAccount}>My Account</Text>
        </View>
        <View style={styles.boxContainerProfile}>
        <View style={styles.iconContainer}>
          <Icon2 name='edit-location' size={28} style={styles.iconContent} />
          <Icon2 name='payment'size={28}  style={styles.iconContent} />
          <Icon name='shopping-bag' size={28}  style={styles.iconContent} />
          <Icon2 name='favorite' size={28}  style={styles.iconContent} />
          <Icon name='settings' size={28} style={styles.iconContent} />
          <Icon2 name='help-outline' size={28} style={styles.iconContent} />
          <Icon name= 'log-out' size={28} style={styles.iconContent} />
        </View>
        <View style={styles.nameIconContainer}>
          <Text style={styles.nameIcon}
              onPress={() => navigation.navigate('Maps')}
            >Manage Address</Text>
          <Text style={styles.nameIcon}>Payment</Text>
          <Text style={styles.nameIcon}>My Orders</Text>
          <Text style={styles.nameIcon}>Favourite</Text>
          <Text style={styles.nameIcon}>Setting</Text>
          <Text style={styles.nameIcon}>Help</Text>
          <Text 
            onPress={() => onLogoutPress()}
            style={styles.nameIcon}
            >Logout</Text>
        </View>
      </View>
      </View>
    </ImageBackground>
  )
}

export default Profile;