import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  profile: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'white',
    margin: 20
  },
  imageProfile: {
    height: 80,
    width: 80,
    borderRadius: 80
  },
  username: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
    paddingTop: 10
  },
  email: {
    fontSize: 14,
    color: 'white',
    fontStyle: 'italic'
  },
  loc:{
    width: 90,
    height: 25,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 5
  },
  nameLoc: {
    fontSize: 14,
    color: 'white',
  },
  boxContent: {
    height: '60%',
    width: '65%',
    backgroundColor: 'white',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    paddingTop: 20,
    paddingHorizontal: 20,
    marginTop: 30
  },
  myAccount: {
    fontSize: 18,
    color: 'black',
    fontWeight: 'bold'
  },
  boxContainerProfile: {
    flexDirection: 'row',
    marginTop: 5
  },
  iconContainer:{
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 5
  },
  iconContent: {
    color: '#840EE1',
    margin: 7
  },
  nameIconContainer: {
    marginHorizontal: 10,
  },
  nameIcon: {
    fontSize: 15,
    color: '#840EE1',
    margin: 6,
    paddingTop: 10
  }
})

export default styles;