import React, {useState} from 'react';
import { View, Text, ImageBackground, TextInput, Button,Modal, Image, TouchableOpacity} from "react-native";
import styles from './style';
import Icon from 'react-native-vector-icons/Feather';
import { RNCamera } from 'react-native-camera';
import storage from '@react-native-firebase/storage';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';


function Register ({navigation}) {
  const [isVisible, setIsVisible] = useState(false)
    const [type, setType] = useState('back')
    const [photo, setPhoto] = useState(null)

    const toggleCamera = () => {
      setType(type === 'back' ? 'front' : 'back')
    }
   
    const takePicture = async () => {
      const options = { quality: 0.5, base64: true}
      if(camera) {
        const data = await camera.takePictureAsync(options)
        setPhoto(data)
        setIsVisible(false)
      }
    }

    const uploadImage = (uri) => {
      const sessionId = new Date().getTime()
      return storage()
      .ref(`${sessionId}`)
      .putFile(uri)
      .then((response) => {
        alert('Upload Succes')
        navigation.navigate('TabsScreen')
      })
      .catch((error) => {
        alert(error)
      })
    }
    const renderCamera = ()=> {
    return ( 
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{flex: 1}}>
          <RNCamera
            style={{ flex: 1}}
            ref={ref => {
              camera = ref;
            }}
            type={type}
          >
            <View style={styles.btnFlipContainer}>
              <TouchableOpacity style={styles.btnFlip} onPress={()  => toggleCamera()}>
                <MaterialCommunity name='rotate-3d-variant' size={20}/>
              </TouchableOpacity>
            </View>
            <View style={styles.round}/>
            <View style={styles.rectangle} />
            <View style={styles.btnTakeContainer}>
              <TouchableOpacity style={styles.btnTake} onPress={() => takePicture()}>
                <Icon name='camera' size={40}/>
              </TouchableOpacity>
            </View>
          </RNCamera>
        </View>
      </Modal>
    )
    }

  return (
    <ImageBackground source={require('../../assets/image/bake.jpg')} style={{ flex: 1, width: '100%', height: '100%', justifyContent: 'flex-end'}}>
      {renderCamera()}
      <View style={styles.boxContainer}>
        <Image source={photo === null ? require('../../assets/image/blank-photo-profile.png') : {uri: photo.uri}} style={{width: 100, height: 100, borderRadius: 100, alignSelf: 'center'}} />
        <TouchableOpacity onPress={()=> setIsVisible(true)}>
        <Text style={styles.changePicture}>Change picture</Text>
      </TouchableOpacity>
        <Text style={{fontWeight: 'bold', color: 'white'}}>Email</Text>
        <TextInput
          placeholder= 'Email'
          underlineColorAndroid= 'white'
          placeholderTextColor='white'
        />
        <Text style={{fontWeight: 'bold', color: 'white', marginTop: 10}}>Password</Text>
        <TextInput
          placeholder= 'Password'
          underlineColorAndroid= 'white'
          placeholderTextColor='white'
        />
        <Text style={{fontWeight: 'bold', color: 'white', marginTop: 10}}>Re-enter Password</Text>
        <TextInput
          placeholder='Re-enter Password'
          underlineColorAndroid='white'
          placeholderTextColor='white'
        />
        <Button
          color='#840EE1'
          title='SIGN UP'
          onPress={() => uploadImage(photo.uri)}
        />
        <Text style={{textAlign: 'center', color: 'black', marginTop: 80}} >Already have an account?
          <Text style={{color: 'white'}} onPress={()=> navigation.navigate('Login')}>  Sign In</Text>
        </Text>
      </View>
    </ImageBackground>
  )
}

export default Register;