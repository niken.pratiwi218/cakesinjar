import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  boxContainer: {
    flex: 0.8,
    width: '100%',
    backgroundColor: 'rgba(224, 42, 227, 0.5)',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    padding: 20,
  },
  changePicture: {
    fontSize: 12,
    textDecorationLine: 'underline',
    color: 'white',
    alignSelf: 'center',
    marginBottom: 10
  },
  btnFlipContainer: {
    backgroundColor: 'white',
    width: 50,
    height: 50,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent:'center',
    margin: 20
  },
  round: {
    height: 330,
    width: 230,
    borderRadius: 150,
    borderColor: 'white',
    borderWidth: 1,
    marginBottom: 60,
    alignSelf: 'center'
  },
  rectangle: {
    height: 150,
    width: 230,
    borderColor: 'white',
    borderWidth: 1,
    alignSelf: 'center',
    marginBottom: 60
  },
  btnTakeContainer: {
    backgroundColor: 'white',
    width: 80,
    height: 80,
    borderRadius: 80,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  }
})

export default styles;