import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import SplashScreen from '../screens/SplashScreen/splashscreen';
import Onboarding from '../screens/Onboarding/onboarding';
import Login from '../screens/Login/login';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Home from '../screens/Home/home';
import Profile from '../screens/Profile/profile';
import Register from '../screens/Register/register'
import Myorder from '../screens/MyOrder/myorder'
import Maps from '../screens/Maps/mymaps'
import AsyncStorage from '@react-native-community/async-storage'
import Chat from '../screens/Chat/chat'


const Stack = createStackNavigator();
const Tabs = createBottomTabNavigator();

const OnboardingNavigation = () => (
  <Stack.Navigator>
    <Stack.Screen name='Onboarding' component={Onboarding} options={{headerShown: false}}/>
    <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
    <Stack.Screen name='TabsScreen' component={TabsScreen} options={{ headerShown: false}}/>
    <Stack.Screen name="Register" component={Register} options={{ headerShown: false }} />
    <Stack.Screen name='Maps' component={Maps} options={{headerShown: false}} />
  </Stack.Navigator>
)

const UserNavigation = () => (
  <Stack.Navigator>
    <Stack.Screen name='TabsScreen' component={TabsScreen} options={{ headerShown: false}}/>
    <Stack.Screen name='Maps' component={Maps} options={{headerShown: false}} />
    <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
    <Stack.Screen name="Register" component={Register} options={{ headerShown: false }} />
  </Stack.Navigator>
)

const LoginNavigation = () => (
  <Stack.Navigator>
    <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
    <Stack.Screen name='TabsScreen' component={TabsScreen} options={{ headerShown: false}}/>
    <Stack.Screen name="Register" component={Register} options={{ headerShown: false }} />
    <Stack.Screen name='Maps' component={Maps} options={{headerShown: false}} />
  </Stack.Navigator>
)

const TabsScreen = () => {
  return(
  <Tabs.Navigator
      tabBarOptions={{
      activeTintColor: '#e91e63',
      showIcon: true
      }}>
      <Tabs.Screen name='Home' component={Home} 
          options={{
              tabBarIcon: ({ tintColor }) => (
                  <Icon name='home' color={tintColor} size={25}/>
              )
          }} 
      />
      <Tabs.Screen name='Cart' component={Myorder}
        options={{
          tabBarIcon: ({ tintColor }) => (
            <Icon name='shopping' color={tintColor} size={25}/>
          )
        }}
      />
      <Tabs.Screen name='Chat' component={Chat}
        options={{
          tabBarIcon: ({ tintColor }) => (
            <Icon name='chat-outline' color={tintColor} size={25}/>
          )
        }}
      />
      <Tabs.Screen name='Profile' component={Profile}
          options={{
              tabBarIcon: ({ tintColor }) => (
                  <Icon name='account-outline' color={tintColor} size={25}/>
          )
      }}
      />
  </Tabs.Navigator>
  )
}

function Navigation() {
  const [ isLoading, setIsLoading] = React.useState(true)
  const [login, setLogin] =React.useState(false)
  const [onBoarding, setOnBoarding] = React.useState(false)

  const getstatus = async() => {
    const login = await AsyncStorage.getItem('login')
    const onBoarding = await AsyncStorage.getItem('Onboarding')
    setLogin(login)
    setOnBoarding(onBoarding)
  }

  React.useEffect(() => {
    getstatus()
    setTimeout(() => {
      setIsLoading(!isLoading)
    }, 2000)
  }, [])

  if(isLoading) {
    return <SplashScreen/>
  }

  if(login === 'true') {
    return (
        <NavigationContainer> 
            <UserNavigation />
        </NavigationContainer>
    )
  } else if (onBoarding === 'true') {
    return (
        <NavigationContainer>
            <LoginNavigation />
        </NavigationContainer>
    )
  } 
  return (
    <NavigationContainer>
      <OnboardingNavigation />
    </NavigationContainer>
  )
};

export default Navigation;